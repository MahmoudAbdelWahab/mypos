@if(isset($errors) && count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
        <button class="close" data-dismiss="alert" area-hidden="true">&times;</button>
        <ul>
            @foreach($errors->all() as $error)
                <li>
                    {{$error}}
                </li>
            @endforeach
        </ul>
    </div>
@endif