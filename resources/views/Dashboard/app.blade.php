<!DOCTYPE html>
<html dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset("assets/css/ionicons.min.css") }}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset("assets/css/tempusdominus-bootstrap-4.min.css") }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset("assets/css/icheck-bootstrap.min.css") }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset("assets/css/jqvmap.min.css") }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset("assets/css/OverlayScrollbars.min.css") }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset("assets/css/daterangepicker.css") }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset("assets/css/summernote-bs4.css") }}">

@if(app()->getLocale()== 'ar')
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset("assets/css/font-awesome-rtl.css") }}">
  <!-- AdminLTE rtl -->
  <link rel="stylesheet" href="{{ asset("assets/css/adminlte-rtl.min.css") }}">
  <!-- AdminLTE rtl -->
  <link rel="stylesheet" href="{{ asset("assets/css/bootstrap-rtl.css") }}">
    {{--<link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css" integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If" crossorigin="anonymous">--}}

@else

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset("assets/css/all.min.css") }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset("assets/css/adminlte.min.css") }}">

@endif

</head>

<body class="hold-transition sidebar-mini layout-fixed">

<div class="wrapper">
    @include("Dashboard.Layouts._header")
    @include("Dashboard.Layouts._aside")
      <div class="content-wrapper">
          @yield("content")
      </div>
    @include("Dashboard.Layouts._footer")
    @include("Dashboard.Layouts._control-sidebar")
    @include("partials._errors")
</div>


  <!-- jQuery -->
  <script src="{{ asset("assets/js/jquery.min.js") }}"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{ asset("assets/js/jquery-ui.min.js") }}"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
//    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset("assets/js/bootstrap.bundle.min.js") }}"></script>
  <!-- ChartJS -->
  <script src="{{ asset("assets/js/Chart.min.js") }}"></script>
  <!-- Sparkline -->
  <script src="{{ asset("assets/js/sparkline.js") }}"></script>
  <!-- JQVMap -->
  <script src="{{ asset("assets/js/jquery.vmap.min.js") }}"></script>
  <script src="{{ asset("assets/js/jquery.vmap.usa.js") }}"></script>
  <!-- jQuery Knob Chart -->
  <script src="{{ asset("assets/js/jquery.knob.min.js") }}"></script>
  <!-- daterangepicker -->
  <script src="{{ asset("assets/js/moment.min.js") }}"></script>
  <script src="{{ asset("assets/js/daterangepicker.js") }}"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="{{ asset("assets/js/tempusdominus-bootstrap-4.min.js") }}"></script>
  <!-- Summernote -->
  <script src="{{ asset("assets/js/summernote-bs4.min.js") }}"></script>
  <!-- overlayScrollbars -->
  <script src="{{ asset("assets/js/jquery.overlayScrollbars.min.js") }}"></script>

  <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>

  <!-- AdminLTE App -->
  <script src="{{ asset("assets/js/adminlte.js") }}"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="{{ asset("assets/js/dashboard.js") }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset("assets/js/demo.js") }}"></script>
</body>
</html>
